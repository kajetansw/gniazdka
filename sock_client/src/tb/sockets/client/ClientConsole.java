package tb.sockets.client;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.Socket;
import java.util.concurrent.Semaphore;

import javax.swing.JButton;

public class ClientConsole implements ActionListener {
	
	Socket clientSocket = null;
	DataInputStream inFromServer = null;
	DataInputStream inFromUser = null;
	PrintStream outToServer = null;
	
	public static final String HOST = "localhost";
	public static final int PORT = 6666;
	
	MainFrame frame;
	
	String queryToServer = "";
	
	Semaphore semaphore = new Semaphore(0);
	
	GameData clientGameData = new GameData();
	GameData serverGameData = new GameData();
	
	public static void main(String[] args) {

		ClientConsole client = new ClientConsole();
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					client.frame = new MainFrame(HOST,PORT,client);
					client.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		client.connect();
		client.sendQuery();
		client.closeAll();
	}
	
	/** Starts the client Socket, connects it with PrintStream and DataInputStream.
	 * 
	 */
	public void connect() {
		try {
			clientSocket = new Socket(HOST,PORT);
			outToServer = new PrintStream(clientSocket.getOutputStream());
			inFromServer = new DataInputStream(clientSocket.getInputStream());
			inFromUser = new DataInputStream(new BufferedInputStream(System.in));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/** Manages sending queries to server and responding to given messages. Infroms the client whether 
	 * he won or lost. Saves data of the game for the client and server sides. Uses Semaphore to wait for 
	 * Client's move (clicking the button on the screen).
	 */
	public void sendQuery() {
		
		if (clientSocket != null && outToServer != null && inFromServer != null) {
			System.out.println("Client started.");
			try {
				semaphore.acquire();
				outToServer.println(queryToServer);
				clientGameData.releaseMove(Integer.parseInt(queryToServer));
				String serverResponse = null;
				while ((serverResponse = (new BufferedReader(new InputStreamReader(inFromServer))).readLine()) != null) {
					System.out.println(serverResponse);
					if (serverResponse.startsWith("sw")) {
						frame.gameButtons[Integer.parseInt(serverResponse.substring(2))].setText("o");
						frame.messageLabel.setForeground(Color.red);
						frame.messageLabel.setText("SERVER IS A WINNER!");
						for (JButton buttons : frame.gameButtons) {
							buttons.setEnabled(false);
						}
						break;
			        }
					if (serverResponse.indexOf("end") != -1) {
						if(clientGameData.isWin()) {
							frame.messageLabel.setForeground(Color.green);
							frame.messageLabel.setText("YOU ARE A WINNER!");
							for (JButton buttons : frame.gameButtons) {
								buttons.setEnabled(false);
							}
							outToServer.println("close");
							break;
						}
						else {
							frame.messageLabel.setForeground(Color.orange);
							frame.messageLabel.setText("IT'S A TIE!");
							outToServer.println("close");
							break;
						}
			        }
					if (serverResponse.indexOf("tie") != -1) {
						frame.messageLabel.setForeground(Color.orange);
						frame.messageLabel.setText("IT'S A TIE!");
						outToServer.println("close");
						break;
			        }
					if(clientGameData.isWin()) {
						frame.messageLabel.setForeground(Color.green);
						frame.messageLabel.setText("YOU ARE A WINNER!");
						for (JButton buttons : frame.gameButtons) {
							buttons.setEnabled(false);
						}
						outToServer.println("close");
						break;
					}
					if(frame.areButtonsEnabled() == false) {
						if(clientGameData.isWin()) {
							frame.messageLabel.setForeground(Color.green);
							frame.messageLabel.setText("YOU ARE A WINNER!");
							for (JButton buttons : frame.gameButtons) {
								buttons.setEnabled(false);
							}
							outToServer.println("close");
							break;
						}
						else {
							frame.messageLabel.setForeground(Color.orange);
							frame.messageLabel.setText("IT'S A TIE!");
							outToServer.println("close");
							break;
						}
					}
					
					serverGameData.releaseMove(Integer.parseInt(serverResponse));
					//gameData.releaseMove(Integer.parseInt(serverResponse));
					frame.gameButtons[Integer.parseInt(serverResponse)].setText("o");
					frame.gameButtons[Integer.parseInt(serverResponse)].setEnabled(false);
					
					semaphore.acquire();
					outToServer.println(queryToServer);
					clientGameData.releaseMove(Integer.parseInt(queryToServer));
					
				}
			} catch (IOException | InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/** Closes all sockets and streams.
	 */
	public void closeAll() {
		try {
			inFromUser.close();
			inFromServer.close();
			outToServer.close();
			clientSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		queryToServer = "";
		JButton button = (JButton) e.getSource();
		queryToServer = button.getActionCommand();
		button.setEnabled(false);
		//button.setForeground(Color.RED);			// nie dzia�a dla wy��czonego przycisku
		//button.setFont(new Font("Arial",Font.BOLD,36));
		button.setText("X");
		semaphore.release();
	}

	
}
