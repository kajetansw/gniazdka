package tb.sockets.client;

import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**Represents tha main frame of the client-server interactions.
 * @author Kajetan �wi�tek
 * @version 1.00
 */
public class MainFrame extends JFrame {
	
	public final int[] buttonIndex = {0,1,2,3,4,5,6,7,8};
	public final JButton[] gameButtons = new JButton[buttonIndex.length];
	
	public JLabel hostLabel = new JLabel();
	public JLabel portLabel = new JLabel();
	public JLabel messageLabel = new JLabel();
	
	ActionListener aListen;
	
	public MainFrame(String host, Integer port, ActionListener a) {

		super("K�ko i krzy�yk: Klient vs serwer");
		this.aListen = a;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100,100,500,500);
		this.setLayout(null);
		
		int x=70, y=120, dx=70, dy=70, m=0;
		for (int i=0; i<buttonIndex.length; i++) {
			gameButtons[i] = new JButton();
			gameButtons[i].setBounds(x+dx*((m++)+1), y+dy, 60, 60);
			gameButtons[i].setFont(new Font("Arial",Font.BOLD,36));
			gameButtons[i].setActionCommand(((Integer)buttonIndex[i]).toString());
			gameButtons[i].addActionListener(aListen);
			this.getContentPane().add(gameButtons[i]);
			if ((i+1)%3 == 0) {
				dy += 70;
				m = 0;
			}
		}
		
		hostLabel.setText("Host: " + host);
		hostLabel.setBounds(70, 30, 100, 25);
		this.getContentPane().add(hostLabel);
		
		portLabel.setText("Nr portu : " + port.toString());
		portLabel.setBounds(70, 60, 100, 25);
		this.getContentPane().add(portLabel);
		
		messageLabel.setText("");
		messageLabel.setFont(new Font("Arial",Font.BOLD,16));
		messageLabel.setBounds(150, 110, 200, 45);
		this.getContentPane().add(messageLabel);
		
	}

	/**Checks whether all the buttons are enabled.
	 * @return true or false
	 */
	public boolean areButtonsEnabled() {
		for (JButton button : gameButtons) {
			if (button.isEnabled()) {
				return true;
			}
		}
		return false;
	}

}
