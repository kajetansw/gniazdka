package tb.sockets.server;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

/** Contains information about player's state of the game, which move is possible in the game 
 * and releases the move if it was made by the player. Also checks if player had won according
 * to the moves he did.
 * @author Kajetan �wi�tek
 * @version 1.00
 */
public class GameData {

	// all possible moves for the players, includes button indexes 0-8
	// and information if move is possible (true) or not (false)
	private LinkedHashMap<Integer, Boolean> moves;
	
	/**
	 * Fills the LinkedHashMap with all possible moves and enables them by deafault.
	 * @see GameData#moves
	 */
	public GameData() {
		moves = new LinkedHashMap<Integer, Boolean>();
		moves.put(0, true);
		moves.put(1, true);
		moves.put(2, true);
		moves.put(3, true);
		moves.put(4, true);
		moves.put(5, true);
		moves.put(6, true);
		moves.put(7, true);
		moves.put(8, true);
	}
	
	/** Changes value of the specified key to 'false'.
	 * @param key - int value of 0-8 index of the button/move that is to be released after
	 * making a move
	 */
	public void releaseMove(int key) {
		if (key < 0 || key > 8) {
			System.err.println("Invalid key! Key must be 0-8 value!");
			return;
		}
		moves.put(key, false);
	}
	
	
	/** Checks if player won according to moves he has made.
	 * @return true or false
	 */
	public boolean isWin() {
		ArrayList<Integer> bufferList = new ArrayList<Integer>();
		Iterator<Entry<Integer, Boolean>> it = moves.entrySet().iterator();
		
		while(it.hasNext()) {
			Entry<Integer, Boolean> buffer = it.next();
			if (buffer.getValue() == false) {
				bufferList.add(buffer.getKey());
			}
		}
		
		if (bufferList.contains(0) && bufferList.contains(1) && bufferList.contains(2)) {
			return true;
		}
		else if (bufferList.contains(3) && bufferList.contains(4) && bufferList.contains(5)) {
			return true;
		}
		else if (bufferList.contains(6) && bufferList.contains(7) && bufferList.contains(8)) {
			return true;
		}
		else if (bufferList.contains(0) && bufferList.contains(3) && bufferList.contains(6)) {
			return true;
		}
		else if (bufferList.contains(1) && bufferList.contains(4) && bufferList.contains(7)) {
			return true;
		}
		else if (bufferList.contains(2) && bufferList.contains(5) && bufferList.contains(8)) {
			return true;
		}
		else if (bufferList.contains(0) && bufferList.contains(4) && bufferList.contains(8)) {
			return true;
		}
		else if (bufferList.contains(2) && bufferList.contains(4) && bufferList.contains(6)) {
			return true;
		}
		
		return false;
	}
	
	/** Checks whether the move is possible (whether value of the key is true or false).
	 * @param key - int value of 0-8 index of the button/move 
	 * @return true or false
	 */
	public boolean isMovePossible(int key) {
		return moves.get(key).booleanValue();
	}
	
	
}
