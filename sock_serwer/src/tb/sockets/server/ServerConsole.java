package tb.sockets.server;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class ServerConsole {
	
	public static void main(String[] args) {
		
		GameData servGameData = new GameData();
		GameData clientGameData = new GameData();
		Random random = new Random();
		
		ServerSocket serverSocket = null;
		Socket clientSocket = null;
		DataInputStream inFromClient;
		PrintStream outToClient;
		int counter = 0;
		
		try {
			serverSocket = new ServerSocket(6666);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Server started!");
		
		try {
			clientSocket = serverSocket.accept();
			inFromClient = new DataInputStream(clientSocket.getInputStream());
			outToClient = new PrintStream(clientSocket.getOutputStream());
			BufferedReader br = new BufferedReader(new InputStreamReader(inFromClient));
			String clientMove = null;
			
			while (true) {
				clientMove = br.readLine();
				counter++;
				
				if (counter>=9) {
					outToClient.println("end");
					break;
				}
				
				if (clientMove.compareTo("close")==0) {
					break;
				}
				
				try {
					Thread.sleep(200);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				System.out.println("Clientmove: " + clientMove);
				clientGameData.releaseMove(Integer.parseInt(clientMove));
				//gameData.releaseMove(Integer.parseInt(clientMove));
				
				int serverMove = random.nextInt(9);
				if (!servGameData.isMovePossible(serverMove) && !clientGameData.isMovePossible(serverMove)) {
					outToClient.println("tie");
					counter++;
					break;
				}
				while (!servGameData.isMovePossible(serverMove) || !clientGameData.isMovePossible(serverMove)) {
					serverMove = random.nextInt(9);
					
				}
				servGameData.releaseMove(serverMove);
				
				if(servGameData.isWin()) {
					outToClient.println("sw"+serverMove);
					counter++;
					break;
				}
				System.out.println("Servermove: " + serverMove);
				outToClient.println(serverMove);
				counter++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			clientSocket.close();
			serverSocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
